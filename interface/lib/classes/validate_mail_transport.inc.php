<?php

/*
Copyright (c) 2007, Till Brehm, projektfarm Gmbh
Copyright (c) 2021, Jesse Norell <jesse@kci.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class validate_mail_transport {

	function get_error($errmsg) {
		global $app;

		if(isset($app->tform->wordbook[$errmsg])) {
			return $app->tform->wordbook[$errmsg]."<br>\r\n";
		} else {
			return $errmsg."<br>\r\n";
		}
	}

	/* Validator function for checking that the 'domain' is not already set as mail_domain */
	function validate_isnot_maildomain($field_name, $field_value, $validator) {
		global $app, $conf;

		if(isset($app->remoting_lib->primary_id)) {
			$id = $app->remoting_lib->primary_id;
		} else {
			$id = $app->tform->primary_id;
		}

		$sql = "SELECT domain_id, domain FROM mail_domain WHERE domain = ? AND domain_id != ?";
		$domain_check = $app->db->queryOneRecord($sql, $field_value, $id);

		if($domain_check) return $this->get_error('domain_is_maildomain');

	}

	/* Validator function for checking that the 'domain' is not already set as mail_transport */
	function validate_isnot_mailtransport($field_name, $field_value, $validator) {
		global $app, $conf;

		if(isset($app->remoting_lib->primary_id)) {
			$id = $app->remoting_lib->primary_id;
		} else {
			$id = $app->tform->primary_id;
		}

		$sql = "SELECT transport_id, domain FROM mail_transport WHERE domain = ? AND transport_id != ?";
		$domain_check = $app->db->queryOneRecord($sql, $field_value, $id);

		if($domain_check) return $this->get_error('domain_is_transport');

	}

	/* Validator function for checking the 'domain' of a mail transport */
	function validate_domain($field_name, $field_value, $validator) {
		global $app, $conf;

		if(isset($app->remoting_lib->primary_id)) {
			$id = $app->remoting_lib->primary_id;
		} else {
			$id = $app->tform->primary_id;
		}

		// mail_transport.domain (could also be an email address) must be unique per server
		$sql = "SELECT transport_id, domain FROM mail_transport WHERE domain = ? AND transport_id != ?";
		$domain_check = $app->db->queryOneRecord($sql, $field_value, $id);

		if($domain_check) return $this->get_error('domain_error_unique');
	}

}
