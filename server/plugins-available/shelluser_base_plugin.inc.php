<?php

/*
Copyright (c) 2007, Till Brehm, projektfarm Gmbh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class shelluser_base_plugin {

	//* $plugin_name and $class_name have to be the same then the name of this class
	var $plugin_name = 'shelluser_base_plugin';
	var $class_name = 'shelluser_base_plugin';
	var $min_uid = 499;
	var $data = array();
	var $web = array();


	//* This function is called during ispconfig installation to determine
	//  if a symlink shall be created for this plugin.
	function onInstall() {
		global $conf;

		if($conf['services']['web'] == true) {
			return true;
		} else {
			return false;
		}

	}


	/*
	 	This function is called when the plugin is loaded
	*/

	function onLoad() {
		global $app;

		/*
		Register for the events
		*/

		$app->plugins->registerEvent('shell_user_insert', $this->plugin_name, 'insert');
		$app->plugins->registerEvent('shell_user_update', $this->plugin_name, 'update');
		$app->plugins->registerEvent('shell_user_delete', $this->plugin_name, 'delete');


	}

	//* This function is called, when a shell user is inserted in the database
	function insert($event_name, $data) {
		global $app, $conf;

		$app->uses('system,getconf');

		$security_config = $app->getconf->get_security_config('permissions');
		if($security_config['allow_shell_user'] != 'yes') {
			$app->log('Shell user plugin disabled by security settings.',LOGLEVEL_WARN);
			return false;
		}

		//* Check if the resulting path is inside the docroot
		$web = $app->db->queryOneRecord("SELECT * FROM web_domain
			LEFT JOIN server_php ON web_domain.server_php_id = server_php.server_php_id
			WHERE `domain_id` = ?", $data["new"]["parent_domain_id"]);

		$this->web = $web;

		if(substr($data['new']['dir'],0,strlen($web['document_root'])) != $web['document_root']) {
			$app->log('Directory of the shell user is outside of website docroot.',LOGLEVEL_WARN);
			return false;
		}
		if(strpos($data['new']['dir'], '/../') !== false || substr($data['new']['dir'],-3) == '/..') {
			$app->log('Directory of the shell user is not valid.',LOGLEVEL_WARN);
			return false;
		}

		if(!$app->system->is_allowed_user($data['new']['username'], false, false)
			|| !$app->system->is_allowed_user($data['new']['puser'], true, true)
			|| !$app->system->is_allowed_group($data['new']['pgroup'], true, true)) {
			$app->log('Shell user must not be root or in group root.',LOGLEVEL_WARN);
			return false;
		}

		if(is_file($data['new']['dir']) || is_link($data['new']['dir'])) {
			$app->log('Shell user dir must not be existing file or symlink.', LOGLEVEL_WARN);
			return false;
		} elseif(!$app->system->is_allowed_path($data['new']['dir'])) {
			$app->log('Shell user dir is not an allowed path: ' . $data['new']['dir'], LOGLEVEL_WARN);
			return false;
		}

		if($data['new']['active'] != 'y' || $data['new']['chroot'] == "jailkit") $data['new']['shell'] = '/bin/false';

		if($app->system->is_user($data['new']['puser'])) {

			// Get the UID of the parent user
			$uid = intval($app->system->getuid($data['new']['puser']));
			if($uid > $this->min_uid) {
				//* Remove webfolder protection
				$app->system->web_folder_protection($web['document_root'], false);

				//* Home directory of the new shell user
				$homedir = $data['new']['dir'].'/home/'.$data['new']['username'];

				// Create home base directory if it does not exist
				if(!is_dir($data['new']['dir'].'/home')){
					$app->file->mkdirs($data['new']['dir'].'/home', '0755');
				}

				// Change ownership of home base dir to root user
				$app->system->chown($data['new']['dir'].'/home','root');
				$app->system->chgrp($data['new']['dir'].'/home','root');
				$app->system->chmod($data['new']['dir'].'/home',0755);

				if(!is_dir($homedir)){
					$app->file->mkdirs($homedir, '0750');
					$app->system->chown($homedir,$data['new']['puser'],false);
					$app->system->chgrp($homedir,$data['new']['pgroup'],false);
				}

				$command = 'useradd -d ? -g ? -o'; // non unique
				$command .= ' -s ? -u ? ?';
				$app->system->exec_safe($command, $homedir, $data['new']['pgroup'], $data['new']['shell'], $uid, $data['new']['username']);

				$app->log("Executed command: ".$command, LOGLEVEL_DEBUG);
				$app->log("Added shelluser: ".$data['new']['username'], LOGLEVEL_DEBUG);

				if($data['new']['password'] != '') {
					$retval = null;
					$stderr = '';
					$app->system->pipe_exec('chpasswd -e ' . escapeshellarg($data['new']['username']), $data['new']['username'] . ':' . $data['new']['password'], $retval, $stderr);
					if($retval != 0) {
						$app->log("Command chpasswd failed for user ".$data['new']['username'] . ' with code ' . $retval . ': ' . $stderr, LOGLEVEL_WARN);
					}
				}

				$app->system->chown($data['new']['dir'], 'root', false);
				$app->system->chgrp($data['new']['dir'], 'root', false);

				// call the ssh-rsa update function
				$app->uses("getconf");
				$this->data = $data;
				$this->_setup_ssh_rsa();

				//* Create .bash_history file
				$app->system->touch($homedir.'/.bash_history');
				$app->system->chmod($homedir.'/.bash_history', 0750);
				$app->system->chown($homedir.'/.bash_history', $data['new']['username']);
				$app->system->chgrp($homedir.'/.bash_history', $data['new']['pgroup']);

				//* Create .profile file
				$app->system->touch($homedir.'/.profile');
				$app->system->chmod($homedir.'/.profile', 0644);

				$profile_content = "if [ -f ~/.bashrc ]
then
	. ~/.bashrc
fi

";
				$app->system->file_put_contents($homedir.'/.profile', $profile_content);
				$app->system->chown($homedir.'/.profile', $data['new']['username']);
				$app->system->chgrp($homedir.'/.profile', $data['new']['pgroup']);

				//* Create the .bashrc.d directory for ease of use and a more customisable bashrc environment
				if(!is_dir($homedir.'/.bashrc.d')){
					$app->file->mkdirs($homedir.'/.bashrc.d', '0750');
					$app->system->chown($homedir.'/.bashrc.d', $data['new']['username']);
					$app->system->chgrp($homedir.'/.bashrc.d', $data['new']['pgroup']);
				}

				//* Specified in FHS 3.0, https://refspecs.linuxfoundation.org/FHS_3.0/index.html
				//* Supported by Systemd/XDG, provides binaries via user ~/.local/bin directory and PATH
				if(!is_dir($homedir.'/.local/bin')){
					$app->file->mkdirs($homedir.'/.local/bin', '0750');
					$app->system->chown($homedir.'/.local', $data['new']['username'], false);
					$app->system->chgrp($homedir.'/.local', $data['new']['pgroup'], false);
					$app->system->chown($homedir.'/.local/bin', $data['new']['username'], false);
					$app->system->chgrp($homedir.'/.local/bin', $data['new']['pgroup'], false);
				}

				if($data['new']['chroot'] != 'jailkit') {
					$this->_add_user_bashrc();
				}

				// Create symlinks for conveniance, SFTP user should not land in an empty dir.
				if(!is_link($homedir.'/web')) symlink('../../web', $homedir.'/web');
				if(!is_link($homedir.'/log')) symlink('../../log', $homedir.'/log');
				if(!is_link($homedir.'/private')) symlink('../../private', $homedir.'/private');

				//* Disable shell user temporarily if we use jailkit
				if($data['new']['chroot'] == 'jailkit') {
					$command = 'usermod -s /bin/false -L ? 2>/dev/null';
					$app->system->exec_safe($command, $data['new']['username']);
					$app->log("Disabling shelluser temporarily: ".$command, LOGLEVEL_DEBUG);
				}

				//* Add webfolder protection again
				$app->system->web_folder_protection($web['document_root'], true);
			} else {
				$app->log("UID = $uid for shelluser:".$data['new']['username']." not allowed.", LOGLEVEL_ERROR);
			}
		} else {
			$app->log("Skipping insertion of user:".$data['new']['username'].", parent user ".$data['new']['puser']." does not exist.", LOGLEVEL_WARN);
		}
	}

	//* This function is called, when a shell user is updated in the database
	function update($event_name, $data) {
		global $app, $conf;

		$app->uses('system,getconf');

		$security_config = $app->getconf->get_security_config('permissions');
		if($security_config['allow_shell_user'] != 'yes') {
			$app->log('Shell user plugin disabled by security settings.',LOGLEVEL_WARN);
			return false;
		}

		//* Check if the resulting path is inside the docroot
		$web = $app->db->queryOneRecord("SELECT * FROM web_domain
			LEFT JOIN server_php ON web_domain.server_php_id = server_php.server_php_id
			WHERE `domain_id` = ?", $data["new"]["parent_domain_id"]);

		$this->web = $web;

		if(substr($data['new']['dir'],0,strlen($web['document_root'])) != $web['document_root']) {
			$app->log('Directory of the shell user is outside of website docroot.',LOGLEVEL_WARN);
			return false;
		}

		if(strpos($data['new']['dir'], '/../') !== false || substr($data['new']['dir'],-3) == '/..') {
			$app->log('Directory of the shell user is not valid.',LOGLEVEL_WARN);
			return false;
		}

		if(!$app->system->is_allowed_user($data['new']['username'], false, false)
			|| !$app->system->is_allowed_user($data['new']['puser'], true, true)
			|| !$app->system->is_allowed_group($data['new']['pgroup'], true, true)) {
			$app->log('Shell user must not be root or in group root.',LOGLEVEL_WARN);
			return false;
		}

		if(is_file($data['new']['dir']) || is_link($data['new']['dir'])) {
			$app->log('Shell user dir must not be existing file or symlink.', LOGLEVEL_WARN);
			return false;
		} elseif(!$app->system->is_allowed_path($data['new']['dir'])) {
			$app->log('Shell user dir is not an allowed path: ' . $data['new']['dir'], LOGLEVEL_WARN);
			return false;
		}

		if($data['new']['active'] != 'y') $data['new']['shell'] = '/bin/false';

		if($app->system->is_user($data['new']['puser'])) {
			// Get the UID of the parent user
			$uid = intval($app->system->getuid($data['new']['puser']));
			if($uid > $this->min_uid) {

				//* Home directory of the shell user
				if($data['new']['chroot'] == 'jailkit') {
					$homedir = $data['new']['dir'];
					$homedir_old = $data['old']['dir'];
				} else {
					$homedir = $data['new']['dir'].'/home/'.$data['new']['username'];
					$homedir_old = $data['old']['dir'].'/home/'.$data['old']['username'];
				}

				$app->log("Homedir New: ".$homedir, LOGLEVEL_DEBUG);
				$app->log("Homedir Old: ".$homedir_old, LOGLEVEL_DEBUG);

				// Check if the user that we want to update exists, if not, we insert it
				if($app->system->is_user($data['old']['username'])) {
					//* Remove webfolder protection
					$app->system->web_folder_protection($web['document_root'], false);

					if($homedir != $homedir_old){
						// Rename dir, in case the new directory exists already.
						if(is_dir($homedir)) {
							$app->log("New Homedir exists, renaming it to ".$homedir.'_bak', LOGLEVEL_DEBUG);
							$app->system->rename($homedir,$homedir.'_bak');
						}

						// Move old directory to new path
						$app->system->rename($homedir_old,$homedir);
						$app->file->mkdirs($homedir, '0750');
						$app->system->chown($homedir,$data['new']['puser']);
						$app->system->chgrp($homedir,$data['new']['pgroup']);
					} else {
						if(!is_dir($homedir)){
							if(!is_dir($data['new']['dir'].'/home')){
								$app->file->mkdirs($data['new']['dir'].'/home', '0755');
								$app->system->chown($data['new']['dir'].'/home','root');
								$app->system->chgrp($data['new']['dir'].'/home','root');
							}
							$app->file->mkdirs($homedir, '0750');
							$app->system->chown($homedir,$data['new']['puser']);
							$app->system->chgrp($homedir,$data['new']['pgroup']);
						}
					}

					$app->system->usermod($data['old']['username'], 0, $app->system->getgid($data['new']['pgroup']), $homedir, $data['new']['shell'], $data['new']['password'], $data['new']['username']);
					$app->log("Updated shelluser: ".$data['old']['username'], LOGLEVEL_DEBUG);

					// call the ssh-rsa update function
					$app->uses("getconf");
					$this->data = $data;
					$this->_setup_ssh_rsa();

					//* Create .bash_history file
					if(!is_file($data['new']['dir']).'/.bash_history') {
						$app->system->touch($homedir.'/.bash_history');
						$app->system->chmod($homedir.'/.bash_history', 0750);
						$app->system->chown($homedir.'/.bash_history', $data['new']['puser']);
						$app->system->chgrp($homedir.'/.bash_history', $data['new']['pgroup']);
					}

					//* Create .profile file
					$app->system->touch($homedir.'/.profile');
					$app->system->chmod($homedir.'/.profile', 0644);
					$profile_content = "if [ -f ~/.bashrc ]
then
	. ~/.bashrc
fi

";
					$app->system->file_put_contents($homedir.'/.profile', $profile_content);
					$app->system->chown($homedir.'/.profile', $data['new']['puser']);
					$app->system->chgrp($homedir.'/.profile', $data['new']['pgroup']);


					//* Create the .bashrc.d directory for ease of use and a more customisable bashrc environment
					if(!is_dir($homedir.'/.bashrc.d')){
						$app->file->mkdirs($homedir.'/.bashrc.d', '0750');
						$app->system->chown($homedir.'/.bashrc.d', $data['new']['username']);
						$app->system->chgrp($homedir.'/.bashrc.d', $data['new']['pgroup']);
					}

					//* Specified in FHS 3.0, https://refspecs.linuxfoundation.org/FHS_3.0/index.html
					//* Supported by Systemd/XDG, provides binaries via user ~/.local/bin directory and PATH
					if(!is_dir($homedir.'/.local/bin')){
						$app->file->mkdirs($homedir.'/.local/bin', '0750');
						$app->system->chown($homedir.'/.local', $data['new']['username'], false);
						$app->system->chgrp($homedir.'/.local', $data['new']['pgroup'], false);
						$app->system->chown($homedir.'/.local/bin', $data['new']['username'], false);
						$app->system->chgrp($homedir.'/.local/bin', $data['new']['pgroup'], false);
					}

					if($data['new']['chroot'] != 'jailkit') {
						$this->_add_user_bashrc();
					}
					//* Add webfolder protection again
					$app->system->web_folder_protection($web['document_root'], true);
				} else {
					// The user does not exist, so we insert it now
					$this->insert($event_name, $data);
				}
			} else {
				$app->log("UID = $uid for shelluser:".$data['new']['username']." not allowed.", LOGLEVEL_ERROR);
			}
		} else {
			$app->log("Skipping update for user:".$data['new']['username'].", parent user ".$data['new']['puser']." does not exist.", LOGLEVEL_WARN);
		}
	}

	function delete($event_name, $data) {
		global $app, $conf;

		$app->uses('system,getconf,services');

		$security_config = $app->getconf->get_security_config('permissions');
		if($security_config['allow_shell_user'] != 'yes') {
			$app->log('Shell user plugin disabled by security settings.',LOGLEVEL_WARN);
			return false;
		}

		if(is_file($data['old']['dir']) || is_link($data['old']['dir'])) {
			$app->log('Shell user dir must not be existing file or symlink.', LOGLEVEL_WARN);
			return false;
		} elseif(!$app->system->is_allowed_path($data['old']['dir'])) {
			$app->log('Shell user dir is not an allowed path: ' . $data['old']['dir'], LOGLEVEL_WARN);
			return false;
		}

		if($app->system->is_user($data['old']['username'])) {
			// Get the UID of the user
			$userid = intval($app->system->getuid($data['old']['username']));
			if($userid > $this->min_uid) {
				$web = $app->db->queryOneRecord("SELECT * FROM web_domain WHERE domain_id = ?", $data['old']['parent_domain_id']);

				// check if we have to delete the dir
				$check = $app->db->queryOneRecord('SELECT shell_user_id FROM `shell_user` WHERE `dir` = ?', $data['old']['dir']);
				if(!$check && is_dir($data['old']['dir'])) {

					$app->system->web_folder_protection($web['document_root'], false);

					// delete dir
					if($data['new']['chroot'] == 'jailkit') {
						$homedir = $data['old']['dir'];
					} else {
						$homedir = $data['old']['dir'].'/home/'.$data['old']['username'];
					}

					if(substr($homedir, -1) !== '/') $homedir .= '/';
					$files = array('.bash_logout', '.bash_history', '.bashrc', '.profile');
					$dirs = array('.ssh', '.cache');
					foreach($files as $delfile) {
						if(is_file($homedir . $delfile) && fileowner($homedir . $delfile) == $userid) unlink($homedir . $delfile);
					}
					foreach($dirs as $deldir) {
						if(is_dir($homedir . $deldir) && fileowner($homedir . $deldir) == $userid) $app->system->exec_safe('rm -rf ?', $homedir . $deldir);
					}
					$empty = true;
					$dirres = opendir($homedir);
					if($dirres) {
						while(($entry = readdir($dirres)) !== false) {
							if($entry != '.' && $entry != '..') {
								$empty = false;
								break;
							}
						}
						closedir($dirres);
					}
					if($empty == true) {
						rmdir($homedir);
					}
					unset($files);
					unset($dirs);

					$app->system->web_folder_protection($web['document_root'], true);
				}

				// We delete only non jailkit users, jailkit users will be deleted by the jailkit plugin.
				if ($data['old']['chroot'] != "jailkit") {
					// if this web uses PHP-FPM, that PHP-FPM service must be stopped before we can delete this user
					if($web['php'] == 'php-fpm'){
						if($web['server_php_id'] != 0){
							$default_php_fpm = false;
							$tmp_php = $app->db->queryOneRecord('SELECT * FROM server_php WHERE server_php_id = ?', $web['server_php_id']);
							if($tmp_php) {
								$custom_php_fpm_ini_dir = $tmp_php['php_fpm_ini_dir'];
								$custom_php_fpm_init_script = $tmp_php['php_fpm_init_script'];
								if(substr($custom_php_fpm_ini_dir, -1) != '/') $custom_php_fpm_ini_dir .= '/';
							}
						} else {
							$default_php_fpm = true;
						}
						$web_config = $app->getconf->get_server_config($conf["server_id"], 'web');
						if(!$default_php_fpm){
							$app->services->restartService('php-fpm', 'stop:'.$custom_php_fpm_init_script);
						} else {
							$app->services->restartService('php-fpm', 'stop:'.$conf['init_scripts'].'/'.$web_config['php_fpm_init_script']);
						}
					}
					$command = 'killall -u ? ; userdel -f ? &> /dev/null';
					$app->system->exec_safe($command, $data['old']['username'], $data['old']['username']);
					$app->log("Deleted shelluser: ".$data['old']['username'], LOGLEVEL_DEBUG);
					// start PHP-FPM again
					if($web['php'] == 'php-fpm'){
						if(!$default_php_fpm){
							$app->services->restartService('php-fpm', 'start:'.$custom_php_fpm_init_script);
						} else {
							$app->services->restartService('php-fpm', 'start:'.$conf['init_scripts'].'/'.$web_config['php_fpm_init_script']);
						}
					}
				}

			} else {
				$app->log("UID = $userid for shelluser:".$data['old']['username']." not allowed.", LOGLEVEL_ERROR);
			}
		} else {
			$app->log("User:".$data['new']['username']." does not exist in in /etc/passwd, skipping delete.", LOGLEVEL_WARN);
		}

	}

	private function _setup_ssh_rsa() {
		global $app;
		$app->log("ssh-rsa setup shelluser_base", LOGLEVEL_DEBUG);
		// Get the client ID, username, and the key
		$domain_data = $app->db->queryOneRecord('SELECT sys_groupid FROM web_domain WHERE web_domain.domain_id = ?', $this->data['new']['parent_domain_id']);
		$sys_group_data = $app->db->queryOneRecord('SELECT * FROM sys_group WHERE sys_group.groupid = ?', $domain_data['sys_groupid']);
		$id = (is_array($sys_group_data) && isset($sys_group_data['client_id']))?intval($sys_group_data['client_id']):0;
		$username= (is_array($sys_group_data) && isset($sys_group_data['name']))?$sys_group_data['name']:'';
		$client_data = $app->db->queryOneRecord('SELECT * FROM client WHERE client.client_id = ?', $id);
		$userkey = (is_array($client_data) && isset($client_data['ssh_rsa']))?$client_data['ssh_rsa']:'';
		unset($domain_data);
		unset($client_data);

		// ssh-rsa authentication variables
		//$sshrsa = $this->data['new']['ssh_rsa'];
		$sshrsa = '';
		$ssh_users = $app->db->queryAllRecords("SELECT ssh_rsa FROM shell_user WHERE parent_domain_id = ?", $this->data['new']['parent_domain_id']);
		if(is_array($ssh_users)) {
			foreach($ssh_users as $sshu) {
				if($sshu['ssh_rsa'] != '') $sshrsa .= "\n".$sshu['ssh_rsa'];
			}
		}
		$sshrsa = trim($sshrsa);
		$usrdir = $this->data['new']['dir'];
		//* Home directory of the new shell user
		if($this->data['new']['chroot'] != 'jailkit') {
			$usrdir = $this->data['new']['dir'].'/home/'.$this->data['new']['username'];
		}
		$sshdir = $usrdir.'/.ssh';
		$sshkeys= $usrdir.'/.ssh/authorized_keys';

		$app->uses('file');
		$sshrsa = $app->file->unix_nl($sshrsa);
		$sshrsa = $app->file->remove_blank_lines($sshrsa, 0);

		// If this user has no key yet, generate a pair
		if ($userkey == '' && $id > 0){
			//Generate ssh-rsa-keys
			$app->uses('functions');
			$app->functions->generate_ssh_key($id, $username);
			$app->log("ssh-rsa keypair generated for ".$username, LOGLEVEL_DEBUG);
		};

		if (!file_exists($sshkeys)){
			// add root's key
			$app->file->mkdirs($sshdir, '0700');

			if(is_file('/root/.ssh/authorized_keys')) $app->system->file_put_contents($sshkeys, $app->system->file_get_contents('/root/.ssh/authorized_keys'));

			// Remove duplicate keys
			$existing_keys = @file($sshkeys, FILE_IGNORE_NEW_LINES);
			$new_keys = (!is_null($userkey))?explode("\n", $userkey):array();
			if(is_array($existing_keys)) {
				$final_keys_arr = @array_merge($existing_keys, $new_keys);
			} else {
				$final_keys_arr = $new_keys;
			}
			$new_final_keys_arr = array();
			if(is_array($final_keys_arr) && !empty($final_keys_arr)){
				foreach($final_keys_arr as $key => $val){
					$new_final_keys_arr[$key] = trim($val);
				}
			}
			$final_keys = implode("\n", array_flip(array_flip($new_final_keys_arr))) . "\n";

			// add the user's key
			$app->system->file_put_contents($sshkeys, $final_keys);
			$app->file->remove_blank_lines($sshkeys);
			$app->log("ssh-rsa authorisation keyfile created in ".$sshkeys, LOGLEVEL_DEBUG);
		}

		//* Get the keys
		$existing_keys = file($sshkeys, FILE_IGNORE_NEW_LINES);
		if(!$existing_keys) {
			$existing_keys = array();
		}
		$new_keys = explode("\n", $sshrsa);
		$old_keys = explode("\n", $this->data['old']['ssh_rsa']);

		//* Remove all old keys
		if(is_array($old_keys)) {
			foreach($old_keys as $key => $val) {
				$k = array_search(trim($val), $existing_keys);
				if ($k !== false) {
					unset($existing_keys[$k]);
				}
			}
		}

		//* merge the remaining keys and the ones fom the ispconfig database.
		if(is_array($new_keys)) {
			$final_keys_arr = array_merge($existing_keys, $new_keys);
		} else {
			$final_keys_arr = $existing_keys;
		}

		$new_final_keys_arr = array();
		if(is_array($final_keys_arr) && !empty($final_keys_arr)){
			foreach($final_keys_arr as $key => $val){
				$new_final_keys_arr[$key] = trim($val);
			}
		}
		$final_keys = implode("\n", array_flip(array_flip($new_final_keys_arr)));

		// add the custom key
		$app->system->file_put_contents($sshkeys, $final_keys);
		$app->file->remove_blank_lines($sshkeys);
		$app->log("ssh-rsa key updated in ".$sshkeys, LOGLEVEL_DEBUG);

		// set proper file permissions
		$app->system->exec_safe("chown -R ?:? ?", $this->data['new']['puser'], $this->data['new']['pgroup'], $sshdir);
		$app->system->exec_safe("chmod 600 ?", $sshkeys);

	}


	function _add_user_bashrc() {
		global $app;

		// Create .bashrc file
		$app->load('tpl');

		$tpl = new tpl();

		// Predefine some template vars
		$tpl->setVar('jailkit_chroot', 'n');
		$tpl->setVar('use_php_path', false);

		$os_type = $app->system->get_os_type();
		if (isset($os_type['type'])) {
			$used_os_type = $os_type['type'];
		} else {
			$used_os_type = 'unknown';
		}

		if($this->data['new']['chroot'] == "jailkit") {
			$is_jailed = true;
		} else {
			$is_jailed = false;
		}

		if($used_os_type == "debian" || $used_os_type == "ubuntu") {
			$tpl->newTemplate("bashrc_user_deb.master");
		} elseif($used_os_type == "redhat") {
			$tpl->newTemplate("bashrc_user_redhat.master");
		} else {
			$tpl->newTemplate("bashrc_user_generic.master");
		}

		$user_home_dir = $this->data['new']['dir'] . '/home/' . $this->data['new']['username'];
		$bashrc = $user_home_dir . '/.bashrc';

		if(($this->web['server_php_id'] > 0) && !empty($this->web['php_cli_binary'])) {
			$php_bin_dir = dirname($this->web['php_cli_binary']);
			$home_php = $user_home_dir . '/.local/bin' . '/php';
			if ($is_jailed === true) {
				$real_php_bin_dir = $this->web['document_root'] . $php_bin_dir;
			} else {
				$real_php_bin_dir = $php_bin_dir;
			}

			if(preg_match('/^(\/usr\/(s)?bin|\/(s)?bin)/', $php_bin_dir)) {
				$tpl->setVar('use_php_path', false);

				if(!is_dir($user_home_dir . '/.local/bin')) $app->system->mkdirpath($user_home_dir . '/.local/bin', 0750, $this->data['new']['username'], $this->data['new']['pgroup']);

				if(!empty($app->system->get_newest_php_bin($real_php_bin_dir))) {
					$fallback_php = $app->system->get_newest_php_bin($real_php_bin_dir);
					$fallback_php_bin = str_replace($this->web['document_root'], '', $fallback_php);

					if(!empty($fallback_php) && file_exists($fallback_php_bin)) {
						if(is_link($home_php) || is_file($home_php) || !file_exists($home_php)) {
							unlink($home_php);
							symlink($fallback_php_bin, $home_php);
							//$app->log("Found " . $fallback_php_bin . " as a fallback for PHP in the jail of ". $this->web['domain'], LOGLEVEL_DEBUG);
						}
					} else {

						if(is_link($home_php) || is_file($home_php) || !file_exists($home_php))
						{
							unlink($home_php);
							symlink($this->web['php_cli_binary'], $home_php);
						} else {
							symlink($this->web['php_cli_binary'], $home_php);
						}
					}
				}

			} else {
				// We rely on $PATH in case that the binaries are located in a separate directory that doesn't match the regex above
				$tpl->setVar('use_php_path', true);
				$tpl->setVar('php_bin_dir', $php_bin_dir);
			}

		} elseif(($this->web['server_php_id'] > 0) && empty($this->web['php_cli_binary'])) {
			$app->log("The PHP cli binary is not set for the selected PHP version. Affected web: " . $this->web['domain'], LOGLEVEL_DEBUG);
		}

		//$bashrc = $this->data['new']['dir'] . '/home/' . $this->data['new']['username'] . '/.bashrc';

		if(@is_file($bashrc) || @is_link($bashrc)) unlink($bashrc);

		file_put_contents($bashrc, $tpl->grab());
		$app->system->chown($bashrc, $this->data['new']['username']);
		$app->system->chgrp($bashrc, $this->data['new']['pgroup']);
		$app->log("Added bashrc script: " . $bashrc, LOGLEVEL_DEBUG);
		unset($tpl);

	}

} // end class

?>
